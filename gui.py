import pygame

pygame.init()

font_size1 = 30
font_type1 = pygame.font.Font('timesdl-italic.ttf', font_size1)

sc = pygame.display.set_mode((800, 800), pygame.RESIZABLE)
clock = pygame.time.Clock()


class Button:
    """ Основной класс всех кнопок """
    # | button_name = Button(ширина, высота, путь к изображению)

    # | button_name.draw(...)
    # |     screen - окно
    # |          x - соотношение по ширине окна (в процентах х:100)
    # |          y - соотношение по высоте окна (в процентах у:100)
    # |     action - названия действия, которе происходит при нажатии на кнопку
    # |       text - текст на кнопке
    # | text_color - цвет текста
    # |stable_pose - фиксированная поза (x, y) в этом случае бесполезны
    def __init__(self, width, height, unhovered_image):

        self.width = width  # ширина кнопки
        self.height = height  # высота кнопки
        self.unhovered_image = pygame.image.load(unhovered_image)  # изображение когда курсор НЕ на кнопке
        self.hovered_image = pygame.image.load(unhovered_image)  # изображение когда курсор на кнопке
        self.pressed = False
        self.pose = [0, 0]
        self.font_size = 10

    ''' Отрисовка и действия кнопки '''
    def draw(self, screen, x, y, action=None, text=None, text_color=(255, 255, 255), stable_pose=None):
        w, h = pygame.display.get_surface().get_size()  # узнаём размер экрана
        mouse_pos = pygame.mouse.get_pos()  # узнаём где курсор
        click = pygame.mouse.get_pressed(3)  # получаем нажатые кнопки мыши
        if stable_pose is None:
            # Позиция для кнопки
            x = x * w / 100
            y = y * h / 100
        else:
            x, y = stable_pose
        self.pose = [x, y]

        # Если курсор на кнопке
        if x < mouse_pos[0] < x + self.width and y < mouse_pos[1] < y + self.height:
            screen.blit(self.hovered_image, [x, y])  # Отображаем кнопку hovered
            # Если на кнопку нажимают левой кнопкой мыши
            # А кнопка в это время не нажата
            # И если действие для неё есть
            if click[0] == 1 and self.pressed is False and action is not None:
                self.pressed = True  # Запомнить, что кнопка нажата уже
                self.act(action)  # Действовать
            elif click[0] != 1:  # Если на кнопку не нажали,
                self.pressed = False  # значит она не нажата (Великие мысли великих людей)

        else:
            screen.blit(self.unhovered_image, [x, y])  # Отображаем кнопку unhovered

        if text is not None:
            # Позиция для текста кнопки
            text_x = x + (self.width / 2) - ((len(text) * self.font_size) / 4) + (self.font_size / 4)
            text_y = y + (self.font_size / 2)
            # Отображаем текст кнопки
            screen.blit(font_type1.render(text, True, text_color), [text_x, text_y])

    @staticmethod
    def act(action):
        if action == 'Остановить мир':
            print("Мир остановлен")

        elif action == 'Ускорить мир':
            print("Мир ускорен")

        elif action == 'Открыть меню построек':
            print("Меню построек открыта")

        elif action == 'Открыть склад':
            print("Склад открыт")

        else:
            print('-------------------------------------')
            print(f'Указанного действия "{action}" не существует.\nПерепроверьте название '
                  f'или допишите его в gui.py в функцию act класса Button')


class Bar:
    """ Основной класс всех шкал """
    # | bar_name = Bar(ширина, высота, путь к шрифту, размер текста, цвет текста, цвет шкалы, цвет фона шкалы)

    # | bar_name.draw(...)
    # |     screen - окно
    # |          x - соотношение по ширине окна (в процентах х:100)
    # |          y - соотношение по высоте окна (в процентах у:100)
    # |       size - значение на сколько шкала заполнена (любое число)
    # |  full_size - максимальное значение шкалы (любое число)
    # |       text - текст на шкале
    # |stable_pose - фиксированная поза (x, y) в этом случае бесполезны
    def __init__(self, width, height, text_font='timesdl-italic.ttf', text_size=30,
                 text_color=(255, 255, 255), front_color=(100, 200, 100),  back_color=(0, 0, 0)):
        self.back_color = back_color
        self.front_color = front_color

        self.width = width
        self.height = height

        self.text_color = text_color
        self.font_size = text_size
        self.text_font = pygame.font.Font(text_font, text_size)

        self.position = [0, 0]

    def draw(self, screen, x=0, y=0, size=0, full_size=0, text="", stable_pose=None):
        w, h = pygame.display.get_surface().get_size()  # узнаём размер экрана

        if stable_pose is None:
            # Позиция для шкалы
            x = x * w / 100
            y = y * h / 100
        else:
            x, y = stable_pose
        self.position = [x, y]

        if size > full_size:
            size = full_size

        back_bar_width = x + self.width
        front_bar_width = size * back_bar_width / full_size

        pygame.draw.rect(screen, self.back_color, [x, y, back_bar_width, self.height])
        pygame.draw.rect(screen, self.front_color, [x, y, front_bar_width, self.height])

        if text is not None:
            # Позиция для текста кнопки
            text_x = x + (self.width/2) - (len(text) * self.font_size)/4
            text_y = y + self.font_size/4
            # Отображаем текст кнопки
            screen.blit(self.text_font.render(text, True, self.text_color), [text_x, text_y])



pauseButton = Button(56, 56, 'gui/buttons/pause_unhovered.png')
speedUpButton = Button(56, 56, 'gui/buttons/speedup_unhovered.png')

buildingsMenuButton = Button(87, 47, 'gui/buttons/buildings-menu_unhovered.png')
stockButton = Button(87, 47, 'gui/buttons/stock_unhovered.png')

peopleBar = Bar(width=320, height=40, text_size=27, back_color=(70, 70, 70))
workersBar2 = Bar(width=320, height=30, text_size=20, back_color=(120, 70, 120), front_color=(70, 120, 120))

while True:
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                exit()

    sc.fill((155, 155, 155))

    buildingsMenuButton.draw(sc, x=55, y=0, action='Открыть меню построек')
    stockButton.draw(sc, x=0, y=0, action='Открыть склад',
                     stable_pose=[buildingsMenuButton.pose[0] + buildingsMenuButton.width + 1,
                                  buildingsMenuButton.pose[1]])

    pauseButton.draw(sc, x=3, y=90, action='Остановить мир')
    speedUpButton.draw(sc, x=0, y=0, action='Ускорить мир',
                       stable_pose=[pauseButton.pose[0] + pauseButton.width + 5,
                                    pauseButton.pose[1]])

    peopleBar.draw(sc, x=4, y=1, size=15, full_size=40, text='15/40')
    workersBar2.draw(sc, x=0, y=0, size=8, full_size=15, text='8/15',
                     stable_pose=[peopleBar.position[0], peopleBar.position[1] + peopleBar.height])

    clock.tick(60)
    pygame.display.update()
