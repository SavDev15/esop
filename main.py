import pygame

pygame.init()

win = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
clock = pygame.time.Clock()

class Cell(pygame.sprite.Sprite):
	def __init__(self, image, x, y):
		super().__init__()
		self.image = image
		self.rect = self.image.get_rect()
		self.rect.x, self.rect.y = x, y

def ld_image(image, scale):
	return pygame.transform.scale(pygame.image.load(image).convert_alpha(),  (38 * scale, 18 * scale))

def generate_cell(xp, yp, size, scale):
	mas = []
	for iy in range(size):
		y = yp - iy * 9 * scale
		x = xp + iy * 19 * scale
		for ix in range(size):
			mas.append(Cell(ld_image("src/ground-cell_1.png", scale), x, y))
			x += 19 * scale
			y += 9 * scale

	return mas

scale = 1

while True:
	for event in pygame.event.get():
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				exit()

		#обработка прокрутки колеса мыши
		if event.type == pygame.MOUSEBUTTONDOWN:
			#увеличение 
			if event.button == 4 and scale <= 10:
				scale += 1

			#уменьшение
			elif event.button == 5 and scale > 1: 
				scale -= 1
		

	win.fill((155, 155, 155))

	for cell in generate_cell(600, 600, 10, scale):
		if cell.rect.collidepoint((pygame.mouse.get_pos())):
			cell = Cell(ld_image("src/ground-cell_2.png", scale), cell.rect.x, cell.rect.y)
		else:
			cell = Cell(ld_image("src/ground-cell_1.png", scale), cell.rect.x, cell.rect.y)
		win.blit(cell.image, cell.rect)

	clock.tick(90)
	pygame.display.update()